<?php

include "lastFM.php";
include "updateDB.php";

if(isset($_GET["track"]) && isset($_GET["artist"])){
    $track = urlencode($_GET["track"]);
    $artist = urlencode($_GET["artist"]);
    $url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=$API_KEY&artist=$artist&track=$track";
    $respostaXML = file_get_contents($url);
    
    // tratar XML
    $xml = simplexml_load_string($respostaXML);
    $albumMbid = (string) $xml->track->album->mbid;
    $trackInfo = array();
    $trackInfo["trackName"] = (string) $xml->track->name;
    $trackInfo["artistName"] = (string) $xml->track->artist->name;
    $trackInfo["albumName"] = (string) $xml->track->album->title;
    
    $requestCoverURL = "http://coverartarchive.org/release/$albumMbid";
    $resposta = @file_get_contents($requestCoverURL);
    if($http_response_header[0] != "HTTP/1.1 404 NOT FOUND"){
        $trackInfo["musicBrainzInfo"] = json_decode($resposta);
    }
    else{
        $trackInfo["musicBrainzInfo"] = array();
        $trackInfo["musicBrainzInfo"]["images"] = array();
        $arr = array();
        $arr["image"] = "Not found";
        $trackInfo["musicBrainzInfo"]["images"][0] = $arr;
    }
    
    $url = "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=$artist&api_key=$API_KEY";
    $respostaXML = file_get_contents($url);
    $xml = simplexml_load_string($respostaXML);
    foreach ($xml->artist->image as $image) {
        if ((string) $image['size'] == 'large') {
            $trackInfo["artistImageURL"] = (string) $image;
            }
    }
    
    $url = "http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=$artist&api_key=$API_KEY";
    $respostaXML = file_get_contents($url);
    $xml = simplexml_load_string($respostaXML);
    $trackInfo["topTrackName"] = (string) $xml->toptracks->track->name;
    
    $url = "http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=$artist&api_key=$API_KEY&limit=3";
    $respostaXML = file_get_contents($url);
    $xml = simplexml_load_string($respostaXML);
    
    $trackInfo["artistTopAlbums"] = array();
    foreach($xml->topalbums->album as $album) {
        array_push( $trackInfo["artistTopAlbums"], (string)$album->name);
    }
   
    echo json_encode($trackInfo);
}

?>